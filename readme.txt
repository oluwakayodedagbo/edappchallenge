I focused primarily on backend using SOLID methodology with in the 4 hours allotted

The front end is vanialla JS, and pretty basic, you can build and run the project as is

The app runs on a memory based DB, it is preset with these users 

login:test
login2:test
login3:test 

you can see what you have to auction, and what is there to be bidded on,
you can make a bid, and that is it, though the backend supports more functionality, i wanted to work within the 4 hour rule.

if i had more time i would have used React for the front, more error checking and back end unit testing, editing user information
showing more user info when getting latest bid, listing bids and so on