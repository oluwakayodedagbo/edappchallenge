﻿using AuctionApp.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Data
{
    public class AuctionDBContext: DbContext
    {
        public AuctionDBContext(DbContextOptions<AuctionDBContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Bid> Bids { get; set; }
        public DbSet<AuctionItem> AuctionItems { get; set; }

    }
}
