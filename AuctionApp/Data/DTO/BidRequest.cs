﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Data.DTO
{
    public class BidRequest
    {
        public int Bidder { get; set; }
        public decimal Amount { get; set; }
        public int AuctionItemId { get; set; }
    }
}
