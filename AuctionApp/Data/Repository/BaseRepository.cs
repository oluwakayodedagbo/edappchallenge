﻿using AuctionApp.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AuctionApp.Data.Repository
{
    public class BaseRepository<T>: IRepository<T> where T : class
    { 

        private readonly AuctionDBContext _dbContext;

        public BaseRepository(AuctionDBContext auctionDBContext) {
            _dbContext = auctionDBContext;
        }


        public async Task Add(T entity)
        {
           await _dbContext.AddAsync(entity);
           await _dbContext.SaveChangesAsync();
        }

        public void Update(T entity)
        {
             _dbContext.Update(entity);
        }

        public void Delete(T entity)
        {
            _dbContext.Remove(entity);
        }

        public async Task<T> GetById(int id)
        {
           return await _dbContext.FindAsync<T>(new object[] { id });
        }

        public async Task<List<T>> GetAll()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<List<T>> GetByExpression(Expression<Func<T,bool>> expression)
        {
           return await _dbContext.Set<T>().Where(expression).ToListAsync();
        }
    }
}
