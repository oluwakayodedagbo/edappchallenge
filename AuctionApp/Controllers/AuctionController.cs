﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionApp.Data.DTO;
using AuctionApp.Data.Repository.Interfaces;
using AuctionApp.Entities;
using AuctionApp.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AuctionApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuctionController : ControllerBase
    {


        private readonly IAuctionItemService _auctionItemService;

        public AuctionController(IAuctionItemService auctionItemService)
        {
            _auctionItemService = auctionItemService;
        }

        [HttpGet("User/{userID}/AuctionItems")]
        public async Task<ActionResult<IEnumerable<AuctionItem>>> AuctionItems([FromRoute]int userID)
        {
            var items = await _auctionItemService.GetAuctionItemsForUser(userID);
            return items;
        }

        [HttpGet("AuctionItemsNotForUser/{userID}")]
        public async Task<ActionResult<IEnumerable<AuctionItem>>> AuctionItemsExceptUser([FromRoute]int userID)
        {
            var items = await _auctionItemService.GetAuctionItemsNotForUser(userID);
            return items;
        }

        [HttpGet("AuctionItem/{itemId}")]
        public async Task<AuctionItem> AuctionItem([FromRoute]int itemId)
        {
            var item = await _auctionItemService.GetAuctionItem(itemId);
            return item;
        }

        [HttpGet("AuctionItem/{itemId}/Bids")]
        public async Task<List<Bid>> Bids([FromRoute]int itemId)
        {
            var item = await _auctionItemService.GetBidsForAuctionItem(itemId);
            return item;
        }

        [HttpGet("AuctionItem/{itemId}/HighestBid")]
        public async Task<Bid> HighestBid([FromRoute]int itemId)
        {
            var item = await _auctionItemService.GetBidsForAuctionItem(itemId);

            return item.OrderBy(c=>c.Price).LastOrDefault();
        }

        [HttpPost("Bid")]
        public async Task<IActionResult> Bid([FromBody]BidRequest request)
        {
            Bid bid = new Bid()
            {
                BidderID = request.Bidder,
                AuctionItemID = request.AuctionItemId,
                Price = request.Amount,
                BidID = new Random().Next()
            };

            await _auctionItemService.SubmitBid(bid);
            return Ok();
        }

    }
}