﻿using AuctionApp.Data.Repository.Interfaces;
using AuctionApp.Entities;
using AuctionApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Services
{
    public class AuctionItemService : IAuctionItemService
    {
        private readonly IRepository<AuctionItem> _auctionItemRepository;
        private readonly IRepository<Bid> _bidRepository;

        public AuctionItemService(IRepository<AuctionItem> auctionItemRepository,
            IRepository<Bid> bidRepository)
        {
            _auctionItemRepository = auctionItemRepository;
            _bidRepository = bidRepository;
        }

        public async Task<List<AuctionItem>> GetAuctionItemsForUser(int userID)
        {
            List<AuctionItem> auctionItems = await _auctionItemRepository.GetByExpression(c => c.SellerID == userID);
           
            return auctionItems.ToList();
        }

        public async Task<List<AuctionItem>> GetAuctionItemsNotForUser(int userID)
        {
            List<AuctionItem> auctionItems = await _auctionItemRepository.GetByExpression(c => c.SellerID != userID);

            return auctionItems.ToList();
        }

        public async Task<List<Bid>> GetBidsForAuctionItem(int auctionItemID)
        {
            List<Bid> bids = await _bidRepository.GetByExpression(c => c.AuctionItemID == auctionItemID);
           
            return bids;
        }

        public async Task<Bid> GetAccpetedBidForAuctionItem(int auctionItemID)
        {
            List<Bid> bids = await _bidRepository.GetByExpression(c => c.AuctionItemID == auctionItemID && c.Accepted);

            return bids.FirstOrDefault();
        }


        public async Task<AuctionItem> GetAuctionItem(int auctionItemID)
        {
            List<AuctionItem> auctionItem = await _auctionItemRepository.GetByExpression(c => c.AuctionItemID == auctionItemID);
            return auctionItem.FirstOrDefault();
        }

        public async Task SubmitBid(Bid bid)
        {
            await _bidRepository.Add(bid);
           
        }
    }
}
