﻿using AuctionApp.Data.Repository.Interfaces;
using AuctionApp.Entities;
using AuctionApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
  

        public UserService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
           
        }

        public async Task<User> GetUser(string userName, string passwordHash)
        {
            List<User> user = await _userRepository.GetByExpression(c => c.UserName == userName && c.PasswordHash == passwordHash);
            return user.FirstOrDefault();
        }

      
    }
}
