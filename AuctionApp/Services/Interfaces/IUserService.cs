﻿using AuctionApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Services.Interfaces
{
    public interface IUserService
    {
        Task<User> GetUser(string userName, string passwordHash);
    }
}
