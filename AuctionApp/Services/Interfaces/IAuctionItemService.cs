﻿using AuctionApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Services.Interfaces
{
    public interface IAuctionItemService
    {
        Task<List<AuctionItem>> GetAuctionItemsForUser(int userID);
        Task<List<Bid>> GetBidsForAuctionItem(int auctionItemID);
        Task<Bid> GetAccpetedBidForAuctionItem(int auctionItemID);
        Task<AuctionItem> GetAuctionItem(int auctionItemID);
        Task<List<AuctionItem>> GetAuctionItemsNotForUser(int userID);
        Task SubmitBid(Bid bid);
    }
}
