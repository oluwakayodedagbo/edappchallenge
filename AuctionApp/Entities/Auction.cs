﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Entities
{
    public class Auction
    {
        [Key]
        public int AuctionID { get; set; }
        public int AuctionOwnerID { get; set; }
        public ICollection<AuctionItem> ActionItems { get; set; }
        public User AuctionOwner { get; set; }

    }
}
