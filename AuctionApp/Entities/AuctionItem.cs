﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Entities
{
    public class AuctionItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuctionItemID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string PhotoURL { get; set; }

        public int SellerID { get; set; }
        
        public int OwnerID { get; set; }
                
        
        public User Seller { get; set; }
        public User Owner { get; set; }
     
        public ICollection<Bid> Bids { get; set; }

       
        

    }
}
