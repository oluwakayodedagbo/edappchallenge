﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionApp.Entities
{
    public class Bid
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BidID { get; set; }
        public int BidderID { get; set; }
        public int AuctionItemID { get; set; }
        public decimal Price { get; set; }
        public bool Accepted { get; set; }

        public User Bidder { get; set; }
        public AuctionItem AuctionItem {get; set;}

    }
}
