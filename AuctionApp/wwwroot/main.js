﻿const loginURL = "/api/user/login"
const auctionItemsURL = "/api/Auction/User/{0}/AuctionItems";
const auctionItemsNotMineURL = "/api/Auction/AuctionItemsNotForUser/{0}";
const postBidUrl = "/api/Auction/Bid";
const hightBidUrl = "/api/Auction/AuctionItem/{0}/HighestBid";
var appState = {}

const myItemsDiv = document.getElementById("myitems");
const bidItemsDiv = document.getElementById("itemsTobid");


function loadData(url, method, payload) {
    if (method == "GET") {
        return fetch(url).then(res => res.json())
    }
    else {
        const options = {
            method: method,
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        return fetch(url, options).
            then(res => res.json()).catch(ex => { error: ex })
    }
}


function login() {
    
    let username = document.forms["login"].username.value;
    let password = document.forms["login"].password.value;
    loadData(loginURL, "POST", { username: username, password: password }).
        then(res => {
            if (!res)
                alert("login failed");
            else {
                appState["userId"] = res.userID;
                appState["username"] = res.username;
                appState["firstName"] = res.firstname;
                appState["lastName"] = res.username;
                hide("login");
                showDashboard(true);
            }
        });
}

function logout() {
    hide("dashboard");
    show("login");
}

function hide(id) {
    document.getElementById(id).classList.add("hide");
}

function show(id) {
    document.getElementById(id).classList.remove("hide");
}


function showDashboard(create) {
    show("dashboard");
    if (create) {
        loadData(auctionItemsURL.replace('{0}', appState.userId), "GET").then(res => {
            if (res) {
                populateAuctionItemList(myItemsDiv, res);
            }
        });
    loadData(auctionItemsNotMineURL.replace('{0}', appState.userId), "GET").then(res => {
        if (res) {
           
            populateAuctionItemList(bidItemsDiv, res, true);
        }
    })
       
    }

}

function populateAuctionItemList(itemsDiv, res, bid) {
    itemsDiv.innerHTML = "";
    for (let i = 0; i < res.length; i++) {
        let item = document.createElement("div");

        let name = document.createElement("div");
        name.innerHTML = "<h4>" + res[i].name + "</h4>";
        let price = document.createElement("div");
        price.innerHTML = res[i].price;
        let photo = document.createElement("div");
        photo.innerHTML = "<img src='" + res[i].photoURL + "' width =200 height=150/>"

        item.classList.add('auctionItem');
        item.appendChild(name);
        item.appendChild(price);
        item.appendChild(photo);

        if (bid) {
            let button = document.createElement("button");
            button.type = "button"
            button.value = "BID !!!"
            button.innerHTML = "BID !!"
            button.addEventListener("click", function () { biding(res[i].auctionItemID) }.bind(window))
            item.appendChild(button);
        }

        itemsDiv.appendChild(item);

    }
}

function biding(itemId) {
    
    loadData(hightBidUrl.replace('{0}', itemId), "GET").then(res => {
        if (res) {
            alert("highest bid is " + res.price + " from " + res.sellerId);

            let amount = prompt("How much are you bidding ?")

            if (amount > 0) {
                let bidItem = {

                    Bidder: appState.userId,
                    AuctionItemId: itemId,
                    Amount: amount
                }
                loadData(postBidUrl, "POST", bidItem).then(res => { if (res) { alert("bid successful") } });
            }
        }
    })
    
}

document.forms["login"].login.addEventListener("click", login)
document.forms["logout"].logout.addEventListener("click", logout)