﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionApp.Data;
using AuctionApp.Data.Repository;
using AuctionApp.Data.Repository.Interfaces;
using AuctionApp.Entities;
using AuctionApp.Services;
using AuctionApp.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AuctionApp
{
    public class Startup
    {

        public static readonly InMemoryDatabaseRoot InMemoryDatabaseRoot = new InMemoryDatabaseRoot();
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            services.AddDbContext<AuctionDBContext>(opt => opt.UseInMemoryDatabase("Auction", InMemoryDatabaseRoot));
           
            services.AddScoped(typeof(IRepository<>),typeof(BaseRepository<>));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuctionItemService, AuctionItemService>();
          
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var serviceProvider = services.BuildServiceProvider();

           

            var dbContext = serviceProvider.GetService<AuctionDBContext>();
            FillData(dbContext);

        }

        private void FillData(AuctionDBContext dbContext)
        {
            dbContext.Users.Add(new User()
            {
                UserID = 1,
                CreatedDate = DateTime.Now,
                FirstName = "Max",
                LastName = "Power",
                PasswordHash = "test",
                UserName = "login"
            });

            dbContext.Users.Add(new User()
            {
                UserID = 2,
                CreatedDate = DateTime.Now,
                FirstName = "Maxine",
                LastName = "Power",
                PasswordHash = "test",
                UserName = "login2"
            });

            dbContext.Users.Add(new User()
            {
                UserID = 3,
                CreatedDate = DateTime.Now,
                FirstName = "Joeb",
                LastName = "Smith",
                PasswordHash = "test",
                UserName = "login3"
            });

            dbContext.Users.Add(new User()
            {
                UserID = 4,
                CreatedDate = DateTime.Now,
                FirstName = "Joeb",
                LastName = "Smith",
                PasswordHash = "test",
                UserName = "login4"
            });

            dbContext.AuctionItems.Add(new AuctionItem()
            {
              AuctionItemID = 1,
              Name = "Giant Fish Tank",
              Price = 100,
              PhotoURL = "https://i.ebayimg.com/images/g/g7QAAOSwPM9ch~36/s-l300.png",
              OwnerID = 3,
              SellerID = 3
            });

            dbContext.AuctionItems.Add(new AuctionItem()
            {
                AuctionItemID = 2,
                Name = "Red Car",
                Price = 10000,
                PhotoURL = "http://www.pngpix.com/wp-content/uploads/2016/06/PNGPIX-COM-Ford-Mustang-Red-Car-PNG-Image.png",
                OwnerID = 3,
                SellerID = 3
            });

            dbContext.AuctionItems.Add(new AuctionItem()
            {
                AuctionItemID = 3,
                Name = "Big TV",
                Price = 200,
                PhotoURL = "https://media3.s-nbcnews.com/j/streams/2013/december/131219/2d10230317-lg-105ub9-1-580.nbcnews-ux-1024-900.jpg",
                OwnerID = 4,
                SellerID = 4
            });

            dbContext.AuctionItems.Add(new AuctionItem()
            {
                AuctionItemID = 4,
                Name = "Pool",
                Price = 2000,
                PhotoURL = "https://www.inquirer.com/resizer/c9I_UVpa8Fl4rx75h3GnulQkAxI=/1400x932/smart/arc-anglerfish-arc2-prod-pmn.s3.amazonaws.com/public/4FZ3FON5FREXJFO25DECVPIOMY.jpg",
                OwnerID = 4,
                SellerID = 4
            });

            dbContext.AuctionItems.Add(new AuctionItem()
            {
                AuctionItemID = 5,
                Name = "Billard Table",
                Price = 2000,
                PhotoURL = "https://images-na.ssl-images-amazon.com/images/I/81864aWUmoL._SL1500_.jpg",
                OwnerID = 2,
                SellerID = 2
            });

            dbContext.Bids.Add(new Bid()
            {
                BidID = 1,
                AuctionItemID = 3,
                BidderID = 1,
                Price = 300
            });

            dbContext.Bids.Add(new Bid()
            {
                BidID = 2,
                AuctionItemID = 2,
                BidderID = 1,
                Price = 30
            });


            dbContext.Bids.Add(new Bid()
            {
                BidID = 3,
                AuctionItemID = 1,
                BidderID = 4,
                Price = 30
            });


            dbContext.Bids.Add(new Bid()
            {
                BidID = 4,
                AuctionItemID = 3,
                BidderID = 4,
                Price = 300
            });


            dbContext.Bids.Add(new Bid()
            {
                BidID = 5,
                AuctionItemID = 1,
                BidderID = 3,
                Price = 30000
            });


            dbContext.Bids.Add(new Bid()
            {
                BidID = 6,
                AuctionItemID = 2,
                BidderID = 1,
                Price = 400
            });

            dbContext.Bids.Add(new Bid()
            {
                BidID = 7,
                AuctionItemID = 2,
                BidderID = 3,
                Price = 400
            });

            dbContext.SaveChanges();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
